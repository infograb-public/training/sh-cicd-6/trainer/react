FROM node:16.14.0-alpine3.15 as build
WORKDIR /app
COPY . /app/

RUN npm install
RUN npm install react-scripts@3.0.1 -g
RUN npm run build

# Prepare nginx
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
